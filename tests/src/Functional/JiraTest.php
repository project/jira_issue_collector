<?php

namespace Drupal\Tests\jira_issue_collector\Functional;

use Drupal\Component\Serialization\Json;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\RoleInterface;

/**
 * Test basic functionality of JIRA Issue Collector module.
 *
 * @group Jira issue collector
 */
class JiraTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['jira_issue_collector'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Path to Javascript file from current module.
   *
   * @var string
   */
  protected string $jiraIssueCollectorScript;

  /**
   * Embed in an existing JavaScript resource (requires JQuery) Url.
   *
   * @var string
   */
  protected string $jiraIssueCollectorUrl;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $permissions = [
      'access administration pages',
      'administer jira issue collector',
    ];

    // User to set up JIRA Issue Collector.
    $admin_user = $this->drupalCreateUser($permissions);
    $this->drupalLogin($admin_user);

    // Example URL to code for a JIRA Issue Collector.
    $this->jiraIssueCollectorScript = $this->container->get('extension.list.module')->getPath('jira_issue_collector') . '/js/jira_issue_collector.js';
    $this->jiraIssueCollectorUrl = 'https://jira-instance/some-path/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs.js?locale=en-US&collectorId=ABCD1234';
  }

  /**
   * Pass if the JIRA Issue Collector IS found on the loaded page.
   */
  protected function assertJiraIssueCollectorPresent(): void {
    $this->assertSession()->responseContains($this->jiraIssueCollectorScript);
    $this->assertSession()->responseContains(Json::encode($this->jiraIssueCollectorUrl));
  }

  /**
   * Pass if the JIRA Issue Collector IS NOT found on the loaded page.
   */
  protected function assertJiraIssueCollectorNotPresent(): void {
    $this->assertSession()->responseNotContains($this->jiraIssueCollectorScript);
    $this->assertSession()->responseNotContains(Json::encode($this->jiraIssueCollectorUrl));
  }

  /**
   * Check presence of the settings page.
   */
  public function testConfiguration(): void {
    // Check for setting page's presence.
    $this->drupalGet('admin/config/system/jira_issue_collector');
    $this->assertSession()->responseContains('Code to insert');
  }

  /**
   * Check that the JIRA Issue Collector code is visible.
   */
  public function testPageVisibility(): void {
    $config = $this->config('jira_issue_collector.settings');

    // Show collector on "every page except the listed pages".
    $config
      ->set('url', $this->jiraIssueCollectorUrl)
      ->set('visibility', 0)
      ->set('pages', "/admin\n/admin/*")
      ->set('roles', [RoleInterface::AUTHENTICATED_ID => RoleInterface::AUTHENTICATED_ID])
      ->save();

    drupal_flush_all_caches();

    $this->drupalGet('');
    $this->assertJiraIssueCollectorPresent();

    // Test whether collector code is not included on pages to omit.
    $this->drupalGet('admin');
    $this->assertJiraIssueCollectorNotPresent();
    $this->drupalGet('admin/config/system');
    $this->assertJiraIssueCollectorNotPresent();

    // Test whether collector code display is properly flipped.
    $config->set('visibility', 1)->save();

    drupal_flush_all_caches();

    $this->drupalGet('admin');
    $this->assertJiraIssueCollectorPresent();
    $this->drupalGet('admin/config/system');
    $this->assertJiraIssueCollectorPresent();
    $this->drupalGet('');
    $this->assertJiraIssueCollectorNotPresent();

    // Test whether collector code is not display for anonymous.
    $this->drupalLogout();
    $this->drupalGet('');
    $this->assertJiraIssueCollectorNotPresent();
  }

  /**
   * Test URL detection from HTML code.
   *
   * Check whether we can detect whether the "Embed directly in the HTML of your
   * website" method was used.
   */
  public function testDirectEmbedUrlDetection(): void {
    $settings = [
      'embed_type' => 'html',
      'code' => '<script type="text/javascript" src="' . $this->jiraIssueCollectorUrl . '"></script>',
    ];
    $this->drupalGet('admin/config/system/jira_issue_collector');
    $this->submitForm($settings, 'Save configuration');
    $this->assertSession()->fieldValueEquals('url', $this->jiraIssueCollectorUrl);
  }

  /**
   * Test URL detection from jQuery code.
   *
   * This checks whether we can extract the URL to the collector from inserted
   * code using the "Embed in an existing JavaScript resource (requires JQuery)"
   * method.
   */
  public function testJqueryEmbedUrlDetection(): void {
    $code = '// Requires jQuery!
      jQuery.ajax({
        url: "' . $this->jiraIssueCollectorUrl . '",
        type: "get",
        cache: true,
        dataType: "script"
      });';

    $settings = [
      'embed_type' => 'javascript',
      'code' => $code,
    ];
    $this->drupalGet('admin/config/system/jira_issue_collector');
    $this->submitForm($settings, 'Save configuration');
    $this->assertSession()->fieldValueEquals('url', $this->jiraIssueCollectorUrl);
  }

  /**
   * Check whether it is possible to inject code via the collector URL.
   */
  public function testXssProtection(): void {
    $url = $this->jiraIssueCollectorUrl . ");alert('XSS');//";

    $this->config('jira_issue_collector.settings')
      ->set('url', $url)
      ->save();

    // Check collector code visibility.
    $this->drupalGet('');
    $this->assertSession()->responseNotContains("alert('XSS')");
  }

  /**
   * Check whether it is possible to add a malicious URL via the collector URL.
   */
  public function testMaliciousScriptUrl(): void {
    $settings = [
      'embed_type' => 'html',
      'code' => '<script type="text/javascript" src="http://jira-instance/malicious-script.js"></script>',
    ];
    $this->drupalGet('admin/config/system/jira_issue_collector');
    $this->submitForm($settings, 'Save configuration');
    $this->assertSession()->fieldValueEquals('url', '');
    $this->assertSession()->responseContains('Unable to determine URL for JIRA Issue Collector script from the code. Please verify that you inserted the correct value.');
  }

}
