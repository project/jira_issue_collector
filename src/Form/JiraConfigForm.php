<?php

namespace Drupal\jira_issue_collector\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;

/**
 * Class that defines Jira Issue Collector configuration form.
 */
class JiraConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['jira_issue_collector.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'jira_issue_collector';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('jira_issue_collector.settings');
    $form['embed_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select embed type'),
      '#options' => [
        'html' => $this->t('HTML'),
        'javascript' => $this->t('Javascript'),
      ],
      '#default_value' => $config->get('embed_type'),
      '#required' => TRUE,
      '#description' => $this->t('Your JIRA Issue Collector embed type. You can find a list of your existing collectors at %url.', ['%url' => 'https://your-jira-installation/secure/admin/ViewGlobalCollectors!default.jspa']),
    ];

    $form['code'] = [
      '#type' => 'details',
      '#title' => $this->t('Embed code'),
      '#open' => TRUE,
    ];

    $form['code']['code'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Code to insert'),
      '#default_value' => $config->get('code'),
      '#required' => TRUE,
      '#description' => $this->t('Your JIRA Issue Collector embed code. You can find a list of your existing collectors at %url.', ['%url' => 'https://your-jira-installation/secure/admin/ViewGlobalCollectors!default.jspa']),
    ];

    $form['code']['url'] = [
      '#type' => 'textarea',
      '#rows' => 2,
      '#disabled' => TRUE,
      '#value' => $config->get('url'),
      '#description' => $this->t('The URL to the javascript code for your JIRA Issue Collector. This has been automatically determined from the code inserted above.'),
      '#states' => [
        'invisible' => [
          ":input[name='url']" => ['value' => ''],
        ],
      ],
    ];

    // Render the role overview.
    $form['role_vis_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Role specific visibility settings'),
      '#open' => TRUE,
    ];

    $options = [];
    foreach (Role::LoadMultiple() as $role) {
      $options[$role->id()] = $role->get('label');
    }
    $form['role_vis_settings']['roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Role specific visibility'),
      '#default_value' => $config->get('roles'),
      '#options' => $options,
      '#description' => $this->t('Show JIRA Issue Collector only for the selected role(s). If none of the roles are selected, the collector will be shown for all roles. If a user has any of the roles checked, the collector will be shown for the user.'),
    ];

    // Page specific visibility configurations.
    $form['page_vis_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Page specific visibility settings'),
    ];

    $access = $this->currentUser()
      ->hasPermission('use PHP for JIRA Issue Collector visibility');
    $visibility = $config->get('visibility');
    $pages = $config->get('pages');

    if ($visibility == 2 && !$access) {
      $form['page_vis_settings'] = [];
      $form['page_vis_settings']['visibility'] = [
        '#type' => 'value',
        '#value' => 2,
      ];
      $form['page_vis_settings']['pages'] = [
        '#type' => 'value',
        '#value' => $pages,
      ];
    }
    else {
      $options = [
        $this->t('Show collector on every page except the listed pages.'),
        $this->t('Show collector on the listed pages only.'),
      ];
      $description = $this->t("Enter one page per line as Drupal paths. The '*'
       character is a wildcard. Example paths are %blog for the blog page and
        %blog-wildcard for every personal blog. %front is the front page.", [
          '%blog' => 'blog',
          '%blog-wildcard' => '/blog/*',
          '%front' => '<front>',
        ]);

      $form['page_vis_settings']['visibility'] = [
        '#type' => 'radios',
        '#title' => $this->t('Page specific visibility'),
        '#options' => $options,
        '#default_value' => $visibility,
      ];
      $form['page_vis_settings']['pages'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Pages'),
        '#default_value' => $pages,
        '#description' => $description,
        '#wysiwyg' => FALSE,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $code = $form_state->getValue('code');
    // JIRA can be hosted at any domain so to detect the URL for the Issue
    // Collector we look for an attribute containing an url with the collectorId
    // argument.
    // Example of valid URL:
    // https://jira-instance/some-path/com.atlassian.jira.collector.plugin.jira
    // -issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin
    // .jira-issue-collector-plugin:issuecollector.js?collectorId=abcd1234.
    if (preg_match('/"(http[^"]+(\?|\&)collectorId=[0-9a-f]+)"/im', $code, $matches) && UrlHelper::isValid($matches[1], TRUE)) {
      $form_state->setValue('url', $matches[1]);
    }
    else {
      $form_state->setErrorByName('code', $this->t('Unable to determine URL for JIRA Issue Collector script from the code. Please verify that you inserted the correct value.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('jira_issue_collector.settings');
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      // Clean-up array items for unchecked checkboxes.
      if ($key === 'jira_issue_collector_roles') {
        $value = array_filter($value);
      }

      $config->set($key, $value);
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
