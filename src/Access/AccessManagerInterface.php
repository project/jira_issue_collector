<?php

namespace Drupal\jira_issue_collector\Access;

/**
 * Interface for determining the collector visibility based on path/roles.
 */
interface AccessManagerInterface {

  /**
   * Determine visibility based on roles.
   *
   * Based on visibility setting this function returns TRUE if JIRA Issue
   * Collector code should be added for the current role and otherwise FALSE.
   *
   * @return bool
   *   Whether visibility is allowed or not.
   */
  public function visibilityRoles(): bool;

  /**
   * Determine visibility based on path.
   *
   * Based on visibility setting this function returns TRUE if JIRA Issue
   * Collector code should be added to the current page and otherwise FALSE.
   *
   * @return bool
   *   Whether visibility is allowed or not.
   */
  public function visibilityPages(): bool;

}
