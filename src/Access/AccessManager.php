<?php

namespace Drupal\jira_issue_collector\Access;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\path_alias\AliasManagerInterface;

/**
 * Determine Jira Issue Collector visibility based on path/roles.
 *
 * @ingroup jira_access
 */
class AccessManager implements AccessManagerInterface {

  /**
   * Constructs the AccessManager.
   *
   * @param \Drupal\path_alias\AliasManagerInterface $aliasManager
   *   The path alias manager.
   * @param \Drupal\Core\Path\PathMatcherInterface $pathMatcher
   *   The path matcher.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPath
   *   The current path stack.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user object.
   */
  public function __construct(
    protected AliasManagerInterface $aliasManager,
    protected PathMatcherInterface $pathMatcher,
    protected ConfigFactoryInterface $configFactory,
    protected CurrentPathStack $currentPath,
    protected AccountProxyInterface $currentUser,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function visibilityRoles(): bool {
    $enabled = FALSE;
    // Filter the array to remove any non-selected roles. They will appear as
    // entries with value set to 0.
    $config_roles = $this->configFactory->get('jira_issue_collector.settings')->get('roles');
    $roles = is_array($config_roles) ? array_filter($config_roles) : NULL;
    $current_user_roles = $this->currentUser->getAccount()->getRoles();

    if (!empty($roles)) {
      // One or more roles are selected for tracking.
      foreach ($current_user_roles as $rid) {
        // Is the current user a member of one role enabled for tracking?
        if (isset($roles[$rid]) && $rid == $roles[$rid]) {
          // Current user is a member of a role that should be shown the
          // JIRA Issue Collector widget.
          $enabled = TRUE;
          break;
        }
      }
    }
    else {
      // No role is selected for tracking, therefor the widget should be shown
      // for all roles.
      $enabled = TRUE;
    }

    return $enabled;
  }

  /**
   * {@inheritdoc}
   */
  public function visibilityPages(): bool {
    $visibility = $this->configFactory->get('jira_issue_collector.settings')->get('visibility');
    $pages = $this->configFactory->get('jira_issue_collector.settings')->get('pages');

    // Match path if necessary.
    if (!empty($pages)) {
      if ($visibility < 2) {
        $current_path = $this->currentPath->getPath();
        $path = $this->aliasManager->getAliasByPath($current_path);

        // Compare with the internal and path alias (if any).
        $page_match = $this->pathMatcher->matchPath($path, $pages);
        if ($path != $current_path) {
          $page_match = $page_match || $this->pathMatcher->matchPath($current_path, $pages);
        }
        // When $visibility has a value of 0, the block is displayed on
        // all pages except those listed in $pages. When set to 1, it
        // is displayed only on those pages listed in $pages.
        $page_match = !($visibility xor $page_match);
      }
      else {
        $page_match = FALSE;
      }
    }
    else {
      $page_match = TRUE;
    }

    return $page_match;
  }

}
