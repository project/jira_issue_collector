(function ($) {
  /**
   * Behaviors for setting summaries on content type form.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches summary behaviors on allowed pages.
   */
  Drupal.behaviors.JiraIssueCollector = {
    attach: (context, settings) => {
      // Use ajax() instead of getScript() as this allows cache to be enabled.
      // This is preferable for performance reasons. The JIRA Issue Collector
      // script should not change much.
      if (typeof settings.jiraIssueCollector !== 'undefined') {
        $.ajax({
          url: settings.jiraIssueCollector.url,
          type: 'get',
          cache: true,
          dataType: 'script',
        });
      }
    },
  };
})(jQuery);
